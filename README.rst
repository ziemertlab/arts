Antibiotic Resistant Target Seeker (ARTS) Overview
====================================================
ARTS is a webserver and analysis pipeline for screening for known and
putative antibiotic resistance markers in order to identify and prioritize
their corresponding biosynthetic gene clusters. ARTS can be installed locally
or you can use the free public webserver located at https://arts.ziemertlab.com

See https://bitbucket.org/ziemertlab/artswebapp for a guide on installing the webserver independantly.

Running ARTS
-------------
ARTS uses a webserver to queue jobs to the analysis pipeline. Details on webserver usage can be
found at: https://arts.ziemertlab.com/help
Alternatively jobs can be run directly using the artspipeline1.py script (see -h for options). Example::

    artspipeline1.py antismashOutput.final.gbk reference/actinobacteria/ -rd myresults/ -cpu 8 -opt kres,phyl


ARTS Analysis server local install
===================================

The analysis server uses a daemon, runjobs.py, which consumes jobs submitted though the web interface.

Quick start with docker and docker compose:
-------------------------------------------
For details on setting up docker and docker-compose see https://docs.docker.com/compose/install/
This will install local instances of the analysis and web server on a unix/linux system.
For a windows installation see https://docs.docker.com/docker-for-windows/
The end result should be an directory with the docker-compose and ".env" file
(On windows renaming a file ".file." will produce ".file", or choose save as "*.*" in notepad)

1) Make an isolated directory and download the docker-compose file to install the pre-built ARTS containers::

    mkdir ARTSdocker && cd ARTSdocker
    wget -O docker-compose.yml https://bitbucket.org/ziemertlab/arts/raw/HEAD/docker-compose-arts.yml


2) Set environment variables for multiprocessing, port number to run webserver, and shared folders of host system (replace /tmp with desired path or use these as the default)::

    echo "ARTS_RESULTS=/tmp" > .env
    echo "ARTS_UPLOAD=/tmp" >> .env
    echo "ARTS_RUN=/tmp" >> .env
    echo "ARTS_CPU=1" >> .env
    echo "ARTS_WEBPORT=80" >> .env


3) Build and start the services (from the ARTSdocker directory)::

    docker-compose up


4) Shutting down services and clear containers from disk storage::

    docker-compose down


Extra) Start services in the background, check for running services,
and shutdown without removing containers from disk::

    docker-compose up -d
    docker ps -a
    docker-compose stop


Source install on Debian 8
---------------------------
The following assumes a python2.7 environment with the pip package manager installed.
It is recommended to use a virtual environment using virtualenv: https://virtualenv.pypa.io/en/stable/installation/

The analysis server will start a local antiSMASH job if cluster annotation is not already provided as input.
See https://bitbucket.org/antismash/antismash for installation instructions.

1) Install required libraries and applications (root / sudo required)::

    apt-get update
    apt-get install -y wget python python-dev python-pip liblzma-dev \
       libxml2-dev libxslt-dev zlib1g-dev libxext-dev libjpeg-dev \
       libfreetype6-dev default-jre-headless mafft git
       git clone https://bitbucket.org/ziemertlab/arts
    cd arts
    pip install -r requirements.txt


2) Install required binaries from dependencies.txt file. Or use pre-compiled linux64bit bins (root / sudo required)::

    tar -xzf linux64_bins.tar.gz -C /usr/local/bin/ && hash -r


3) Edit configuration file to define server to listen for job submissions, antismash location, and custom folder paths

4) Start the analysis daemon (see -h for options)::

    python runjobs.py -pid /tmp/runjobs.pid

Support
--------

If you have any issues please feel free to contact us at arts-support@ziemertlab.com

Licence
--------
This software is licenced under the GPLv3. See LICENCE.txt for details.
